﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp_Forms_Exercicio1.Models
{
    public class Usuario
    {
        [Required(ErrorMessage ="Campo NOME obrigatório.")]
        [MaxLength(10, ErrorMessage = "Tamanho máximo do nome: 10 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Campo SOBRENOME obrigatório.")]
        [MaxLength(10, ErrorMessage = "Tamanho máximo do sobrenome: 10 caracteres.")]
        public string Sobrenome { get; set; }

        [Required(ErrorMessage = "Campo IDADE obrigatório.")]
        [Range(18,100, ErrorMessage = "Idade deve ser entre 18 e 100 anos.")]
        public int Idade { get; set; }

        [Required(ErrorMessage = "Campo LOGIN obrigatório.")]
        [MinLength(4, ErrorMessage = "Tamanho mínimo do login: 04 caracteres.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Campo E-MAIL obrigatório.")]
        [EmailAddress(ErrorMessage = "Formato do e-mail inválido.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Campo PERFIL obrigatório.")]
        public string Perfil { get; set; }

        [Required(ErrorMessage = "Campo SENHA obrigatório.")]
        [MinLength(8, ErrorMessage = "Tamanho mínimo do login: 08 caracteres.")]
        [DataType("Password")]
        public string Password { get; set; } = string.Empty;

        [Required(ErrorMessage = "Campo CONFIRMAÇÃO DE SENHA obrigatório.")]
        [Compare("Password", ErrorMessage = "A confirmação de senha deve ser igual a senha.")]
        [DataType("Password")]
        public string ConfirmPassword { get; set; } = string.Empty;
    }
}
