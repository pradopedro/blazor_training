﻿using System.ComponentModel.DataAnnotations;

namespace BlazorApp_Forms.Models
{
    public class Usuario
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo Nome é obrigatório.")]
        [StringLength(10, ErrorMessage = "O nome não pode exceder 10 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "O campo E-mail é obrigatório.")]
        [EmailAddress(ErrorMessage = "Formato do e-mail inválido.")]
        public string Email { get; set; }

        [Range(18,80, ErrorMessage = "A idade deve ser entre 18 e 80.")]
        public int Idade { get; set; }

    }
}
