﻿using BlazorApp_Tasks.Models;

namespace BlazorApp_Tasks.Repositorios
{
    public interface IRepositorio
    {
        List<Tarefa> ObterTarefas();
    }
}
