﻿using BlazorApp_Tasks.Models;

namespace BlazorApp_Tasks.Repositorios
{
    public class Repositorio : IRepositorio
    {
        public List<Tarefa> ObterTarefas() =>
        new List<Tarefa>()
            {
                new Tarefa() {
                    ID = new Guid(),
                    Descricao = "teste",
                    Concluida = false,
                    DataCriacao = Convert.ToDateTime("2022-02-02") 
                },
                new Tarefa() {
                    ID = new Guid(),
                    Descricao = "teste2",
                    Concluida = false,
                    DataCriacao = Convert.ToDateTime("2022-02-02")
                }
            };
        
    }
}
