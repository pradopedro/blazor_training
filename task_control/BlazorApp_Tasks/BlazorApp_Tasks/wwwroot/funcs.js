﻿window.MostrarAlerta = (message) => {
    alert(message);
}

function getTotalTarefas() {
    DotNet.invokeMethodAsync("BlazorApp_Tasks", "ObterTotalTarefas")
        .then(resultado => {
            alert("Total de tarefas: " + resultado);
        });
}

function getTotalTarefasInstancia(dotnet) {
    return dotnet.invokeMethodAsync("ObterTotalTarefasInstancia")
        .then(resultado => {
            alert("Total de tarefas: " + resultado);
        });
}
