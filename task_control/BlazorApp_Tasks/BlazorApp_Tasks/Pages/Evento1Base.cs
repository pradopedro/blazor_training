﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace BlazorApp_Tasks.Pages
{
    public class Evento1Base : ComponentBase
    {
        protected string cor = "red";
        protected void TeclaPressionada(KeyboardEventArgs args)
        {
            cor = (args.Key == "A" || args.Key == "a") ? "blue" :
                        (args.Key == "E" || args.Key == "e") ? "orange" :
                        (args.Key == "I" || args.Key == "i") ? "yellow" :
                        (args.Key == "O" || args.Key == "o") ? "green" :
                        (args.Key == "U" || args.Key == "u") ? "purple" : "red";
        }
    }
}
