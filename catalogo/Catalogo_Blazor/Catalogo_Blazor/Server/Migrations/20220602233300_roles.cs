﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Catalogo_Blazor.Server.Migrations
{
    public partial class roles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "4f27527b-b757-4bb0-a819-5e00193fef18", "61aefb2f-ef33-4ea3-9931-f2422fe2626b", "User", "USER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "7e55e5ec-f0fc-4a35-80b1-cac10fb24292", "31ae7ead-51a5-40a6-a207-78403ec15931", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4f27527b-b757-4bb0-a819-5e00193fef18");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "7e55e5ec-f0fc-4a35-80b1-cac10fb24292");
        }
    }
}
