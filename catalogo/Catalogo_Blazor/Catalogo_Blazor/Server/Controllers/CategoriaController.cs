﻿using Catalogo_Blazor.Server.Context;
using Catalogo_Blazor.Shared.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Catalogo_Blazor.Shared.Recursos;
using Catalogo_Blazor.Server.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Catalogo_Blazor.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CategoriaController : ControllerBase
    {
        private readonly AppDbContext context;

        public CategoriaController(AppDbContext context)
        {
            this.context = context;
        }
        [HttpGet("all")]
        public async Task<ActionResult<List<Categoria>>> GetALL() => await context.Categorias.AsNoTracking().ToListAsync();



        [HttpGet]
        //[AllowAnonymous]
        public async Task<ActionResult<List<Categoria>>> GetCategorias([FromQuery] Paginacao paginacao,
            [FromQuery] string? nome)
        {
            var queryable = context.Categorias.AsQueryable();

            if (!string.IsNullOrEmpty(nome))
                queryable = queryable.Where(x => x.Nome.Contains(nome));


            await HttpContext.InserirParametroEmPageResponse(queryable, paginacao.QuantidadePorPagina);
            return await queryable.Paginar(paginacao).ToListAsync();
        }

       
        [HttpGet("{id}", Name = "GetCategoria")]
        public async Task<ActionResult<Categoria>> GetByIdCategoria(int id) => await context.Categorias
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.CategoriaId == id);

        [HttpPost]
        public async Task<ActionResult<Categoria>> CreateCategoria(Categoria categoria)
        {
            context.Add(categoria);
            await context.SaveChangesAsync();
            return new CreatedAtRouteResult("GetCategoria",
                new { id = categoria.CategoriaId }, categoria);
        }

        [HttpPut]
        public async Task<ActionResult<Categoria>> UpdateCategoria(Categoria categoria)
        {
            context.Entry(categoria).State = EntityState.Modified;
            await context.SaveChangesAsync();
            return Ok(categoria);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Categoria>> DeleteCategoria(int id)
        {
            var categoria = new Categoria { CategoriaId = id };
            context.Remove(categoria);
            await context.SaveChangesAsync();
            return Ok(categoria);
        }
    }
}
