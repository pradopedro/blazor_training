﻿using Microsoft.AspNetCore.Components.Authorization;
using System.Security.Claims;

namespace Catalogo_Blazor.Client.Auth
{
    public class DemoAuthStateProvider : AuthenticationStateProvider
    {
        public override async Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            //indicar se o user está autenticado e seus claims

            var user = new ClaimsIdentity(new List<Claim>() {
            new Claim("key","value"),
            new Claim(ClaimTypes.Name,"pedro"),
            new Claim(ClaimTypes.Role, "Admin")
            },
            "demo");

            return await Task.FromResult(new AuthenticationState(
                   new ClaimsPrincipal(user)));
        }
    }
}
