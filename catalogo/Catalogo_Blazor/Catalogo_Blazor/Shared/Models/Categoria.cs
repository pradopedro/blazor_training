﻿using System.ComponentModel.DataAnnotations;

namespace Catalogo_Blazor.Shared.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }

        [Required(ErrorMessage = "O nome da categoria é obrigatório.")]
        [MaxLength(50, ErrorMessage = "O nome não pode ultrapassar 50 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "A descrição da categoria é obrigatório.")]
        [MaxLength(200, ErrorMessage = "A descrição não pode ultrapassar 200 caracteres.")]
        public string Descricao { get; set; }
        
        public ICollection<Produto> Produtos { get; set; } = new List<Produto>();

    }
}
