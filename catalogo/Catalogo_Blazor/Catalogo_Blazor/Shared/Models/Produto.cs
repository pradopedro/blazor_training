﻿using System.ComponentModel.DataAnnotations;

namespace Catalogo_Blazor.Shared.Models
{
    public class Produto
    {
        public int ProdutoId { get; set; }

        [MaxLength(50)]
        public string Nome { get; set; }

        [MaxLength(200)]
        public string Descricao { get; set; }
        public decimal Preco { get; set; }

        
        public string ImagemUrl { get; set; }
        //relacionamento
        public int CategoriaId { get; set; }
        //navegação
       // public virtual Categoria Categoria { get; set; }

    }
}
