﻿namespace BlazorApp_CascadingValues.Utils
{
    public class Config
    {
        public string CorDeFundo { get; set; } = "orange";
        public string TamanhoDaFonte { get; set; } = "60px";
    }
}
